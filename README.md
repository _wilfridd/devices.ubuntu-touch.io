# Ubuntu Touch devices

The new (new) [Ubuntu Touch devices website](https://devices.ubuntu-touch.io)! Made using [Gridsome](https://gridsome.org/). You can start a development server by running `npm run develop`.

## Where did my device configuration go?

Device files have been moved to the [data/devices](data/devices) folder.

## Where did my Markdown content go? It's not displaying on the live webpage!

We have currently disabled Markdown rendering on each device's page given the changes in how device information is presented. Once we have most of the devices moved from the old (everything in the markdown) method to the new (most information in the YAML) method, we will re-enable rendering.

The [Adding new devices section](#adding-new-devices) will help you update your device's configuration file to the new format.

## Adding new devices

You can add new devices by creating a Markdown file with a YAML metadata block under `data/devices/<codename>.md`. The YAML metadata block is a snippet of YAML in your otherwise Markdown-formatted document, and is denoted by three dashes alone on the line above and below the block.

```md
---
name: My Ported Device
deviceType: phone
...
---

## You should know...

This device occasionally requests an offering of bacon. Don't ask, just provide.

## Preinstallation instructions

Before using the UBports Installer on this device, ...
```

In case your device has many codenames or you are grouping devices you can add other codenames as aliases in the YAML. You should use the same codename as in the installer for the filename. The primary codename is not needed in the aliases.

```yaml
aliases:
  - "second-codename"
  - "third-codename"
```

There is a complete example of a device markdown file at the bottom of this README. Copy that document and fill in your device's information as needed. Note that some optional sections which are listed in the [YAML metadata block reference](#yaml-metadata-block-reference) are not included in the example document. Carefully review the available information and fill in as much as you can.

### YAML block

Your YAML metadata block will have five primary sections. For more information on filling these sections, see the [YAML metadata block reference](#yaml-metadata-block-reference) below.

#### Device description

```yaml
name: "My Cool Device"
deviceType: "phone"
description: "My Cool Device features an absurd 100:1 screen ratio that ensures you never have to scroll a webpage."
buyLink: "https://example.com"
```

The root level of the YAML contains base information about the device, such as its name and type.

#### Device specifications

```yaml
deviceInfo:
  - id: "chipset"
    value: "Qualcomm MSM8956 Snapdragon 650"
```

The deviceInfo section contains an overview of the device's specifications.

#### Compatibility information

Information about working or broken features should be placed in the portStatus section. See [Feature IDs](#feature-ids) for examples and a full list of available feature IDs.

#### Contributors

```yaml
contributors:
  - name: Yumi
    photo: <link to avatar picture>
    forum: <link to ubports forum profile>
```

This is a place to credit the people who have worked on this device port. List them by name and their UBports Forum profile link. If they have a profile picture set on the forum, copy the image link for that picture and add it as the `photo` key. Otherwise, a 3D render of the Yumi mascot for UBports will be used instead.

#### External links, community help and doc links

```yaml
externalLinks:
  - name: "Issue tracker"
    link: "https://github.com/ubports/ubuntu-touch/issues/"
    icon: "github"
```

External links help visitors find more information about your device port. You should have at least two links in this section:

* An issue tracker, where people can report problems they're having with your port. If you're using github.com/ubports/ubuntu-touch, make sure its admins have set you up to access your bug reports there. Otherwise, use an issue tracker that you are comfortable with and have access to.
* Links to the device's source repositories. All relevant source repositories (device, device_common, kernel, manifest) should be linked if possible. Or, even better, add links to all of the relevant repositories from the manifest or device repository and just link to that location. This is what was done for the Sony Xperia X, for example. See [fredldotme/device-sony-suzu](https://github.com/fredldotme/device-sony-suzu) for an example of linking to all the relevant repositories.

Community help links help visitors to get help with using the device. The following link should be added to this section:

* A place to discuss your device port. Usually this will be a category on the UBports Forum. If you would like a forum category for your device, make a post in the forum's General section.

Doc links are links to UBports docs and blog posts that mention the device or might be useful to the user. These links don't need an icon.

The `icon` key should contain the name of an icon which suits the given link. For a full list of usable icons, browse to the [static/img/icons/](static/img/icons/) folder in this repository. Only the link's filename should be used, the file extension is not required. For example, if you would like to use the GitHub icon ([static/img/icons/github.svg](static/img/icons/github.svg)), enter `icon: "github"`.

### Markdown block

```md
---

## This is the Markdown block

I can write Markdown content here. Have [a link](https://example.com)!
```

The markdown block of your file will be rendered as installation instructions on your device's page. For example, if your device needs to have a certain version of Android installed prior to running the UBports Installer, those instructions should be written in Markdown after the metadata block.

Do not place a description or a marketing blurb in the markdown block. It will look out of place. Use the `description` key in the YAML metadata block instead.

Rendering of this Markdown is currently disabled. We will re-enable it after most of the devices listed on the site have migrated to the new format. If you need to show some important information to the potential users of your device, you can manually enable markdown rendering. To enable markdown rendering set the property `enableMdRendering` to `true` in the device yaml.

## YAML metadata block reference

The YAML metadata block should have the following format; variables in square brackets are `[optional]`:

```yml
name: <retail name of the device>
deviceType: <phone|tablet|tv|other>
[description: <marketing-style description that helps a user decide if this device is right for them>]
[installLink: <link to install instructions (repo), if installer isn't available>]
[buyLink: <link to manufacturer's store, if it is available>]
[disableBuyLink: <true|false>]
[price: <average price of the device in dollars>]
[subforum: <the subforum id on forums.ubports.com, should be added in the format number/my-device>]
[tag: <promoted|unmaintained>]
[
aliases:
  - "second-codename"
  - "third-codename"
]
[
portStatus:
  - categoryName: <name of the category of the features>
    features:
      - id: <feature ID, you can find the complete list below>
        value: < + | - | +- | x >
        # +: Working
        # -: Not working (consider linking a bugTracker entry)
        # +-: Partially working (link a relevant bugTracker entry)
        # x: Device does not have hardware support for this feature
        # ?: You have not tested this feature
        bugTracker: <link to the relevant issue report if this feature is not working>
]
[
deviceInfo:
  - id: <device specification ID, you can find the complete list below>
    value: <device technical specification, model...>
]
[
contributors:
  - name: <contributor name>
    photo: <link to avatar picture>
    forum: <link to ubports forum profile>
]
[
communityHelp: # Links for the user to get help during installation and usage
  - name: <link label>
    link: <target>
    icon: <icon for the link> # Available icons can be found in the static/img/ folder
]
[
externalLinks: # Development resources
  - name: <link label>
    link: <target>
    icon: <icon for the link> # Available icons can be found in the static/img/ folder
]
[
docLinks: # Links to UBports docs and blog posts
  - name: <link label>
    link: <target>
]
[
seo:
  # The SEO section will be filled in by site maintainers, there is no need to
  # add content as a porter.
  description: <Page description, used as content in <meta name="description" content="">>
  keywords: <Comma-separated list of keywords, used as content in <meta name="keywords" content="">>
]
```

The subforum id is necessary to display forum posts in the sidebar. To add it search for the subforum on [the Ubports forum](https://forums.ubports.com). The url should look like this "https://forums.ubports.com/category/number/my-awesome-device". Take only the last two parameters in the url bar that should be in the format "number/my-awesome-device".

### Feature IDs

```yaml
portStatus:
  - categoryName: "Sound"
    - id: "earphones"
      value: "-"
      bugTracker: "https://link to my bug report"
```

The portStatus matrix allows the site to display the hardware compatibility of your port. This helps users discover ports that meet their needs and sets expectations for people looking to buy a device for Ubuntu Touch.

If your device has hardware support for a given feature and it can be used under Ubuntu Touch, set the `value:` key to `"+"`. This marks the feature as working.

If your device does not have hardware support for a given feature, set the `value:` key to `"x"`. The site will ignore any missing features when calculating the progress value for your device.

If your device has hardware support for a feature but it does not work when running Ubuntu Touch, set the `value:` key to `"-"`. This marks the feature as broken.

Mark a feature as partially working if your device has hardware support for the feature *and* it works most of the time when running Ubuntu Touch, *but* most users will encounter a critical issue with the feature within 1 week of booting the device. The `value:` key for partially working features is `"+-"`

If you haven't tested a feature or you don't know its state you can set the `value:` key to `"?"`.

If possible, include a bug report link with the `bugTracker` key when marking a feature as broken or partially working.

#### Actors

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| manualBrightness     | Manual brightness                       |
| notificationLed      | Notification LED                        |
| torchlight           | Torchlight                              |
| vibration            | Vibration                               |

#### Camera

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| flashlight           | Flashlight                              |
| photo                | Photo                                   |
| video                | Video                                   |
| switchCamera         | Switching between cameras               |

#### Cellular

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| carrierInfo          | Carrier info, signal strength           |
| dataConnection       | Data connection                         |
| dualSim              | Dual SIM functionality                  |
| calls                | Incoming, outgoing calls                |
| mms                  | MMS in, out                             |
| pinUnlock            | PIN unlock                              |
| sms                  | SMS in, out                             |
| audioRoutings        | Change audio routings (Enable speakerphone) |
| voiceCall            | Voice in calls                          |
| volumeControl        | Volume control in calls                 |

#### Endurance

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| batteryLifetimeTest  | Battery lifetime > 24h from 100% (Suspended, Wi-Fi, mobile data on if available) |
| noRebootTest         | No reboot needed for 1 week             |

#### GPU

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| uiBoot               | Boot into UI                            |
| videoAcceleration    | Hardware video playback                 |

#### Misc

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| anboxPatches         | Anbox patches                           |
| apparmorPatches      | AppArmor patches                        |
| batteryPercentage    | Battery percentage                      |
| offlineCharging      | Offline charging                        |
| onlineCharging       | Online charging                         |
| recoveryImage        | Recovery image                          |
| factoryReset         | Reset to factory defaults               |
| rtcTime              | RTC time                                |
| sdCard               | SD card detection and access            |
| shutdown             | Shutdown / Reboot                       |
| wirelessCharging     | Wireless charging                       |
| wirelessExternalMonitor | Wireless external display support (Miracast) |
| waydroid             | Waydroid support                        |
| fmRadio              | FM Radio                                |

#### Network

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| bluetooth            | Bluetooth                               |
| flightMode           | Flight mode                             |
| hotspot              | Hotspot                                 |
| nfc                  | NFC                                     |
| wifi                 | WiFi                                    |

#### Sensors

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| autoBrightness       | Automatic brightness                    |
| fingerprint          | Fingerprint reader                      |
| gps                  | GPS                                     |
| proximity            | Proximity                               |
| rotation             | Rotation                                |
| touchscreen          | Touchscreen                             |

#### Sound

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| earphones            | Earphones                               |
| loudspeaker          | Loudspeaker                             |
| microphone           | Microphone                              |
| volumeControl        | Volume control                          |

#### USB

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| mtp                  | MTP access                              |
| adb                  | ADB access                              |
| wiredExternalMonitor | Wired external display support (HDMI, DisplayPort, SlimPort, MHL) |

### Device specification IDs

```yaml
deviceInfo:
  - id: "chipset"
    value: "Qualcomm MSM8956 Snapdragon 650"
```

| Specification ID     | Specification name                      |
| :------------------- | :-------------------------------------: |
| cpu                  | CPU                                     |
| chipset              | Chipset                                 |
| gpu                  | GPU                                     |
| rom                  | Storage                                 |
| ram                  | Memory                                  |
| android              | Android Version                         |
| battery              | Battery                                 |
| display              | Display                                 |
| rearCamera           | Rear Camera                             |
| frontCamera          | Front Camera                            |
| arch                 | Architecture                            |
| dimensions           | Dimensions                              |
| weight               | Weight                                  |

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Example device.md

```yaml
---
name: "Your Ported Device"
deviceType: "phone"
portType: "Halium 9.0"
description: "My Cool Device features an absurd 100:1 screen ratio that ensures you never have to scroll a webpage."
installLink: "https://github.com"
price: 200
subforum: "46/my-awesome-device"
aliases:
  - "second-codename"
  - "third-codename"
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "dualSim"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "+"
      - id: "wirelessExternalMonitor"
        value: "+"
      - id: "waydroid"
        value: "+"
      - id: "fmRadio"
        value: "+-"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
      - id: "nfc"
        value: "-"
        bugTracker: "https://github.com/ubports/ubuntu-touch/issues/245"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "+-"
        bugTracker: "https://github.com/..."
deviceInfo:
  - id: "cpu"
    value: "Hexa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm MSM8956 Snapdragon 650"
  - id: "gpu"
    value: "Qualcomm Adreno 510"
  - id: "rom"
    value: "32/64GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "Android 6.0.1"
  - id: "battery"
    value: "2620 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5.5 in"
  - id: "rearCamera"
    value: "23MP"
  - id: "frontCamera"
    value: "13MP"
contributors:
  - name: Me
    photo: https://gravatar.com/me
    forum: https://forums.ubports.com/user/testmoderator01
communityHelp:
  - name: "Telegram"
    link: "https://t.me/joinchat/"
    icon: "telegram"
  - name: "Device Subforum"
    link: "https://forums.ubports.com/"
    icon: "yumi"
externalLinks:
  - name: "Report a bug"
    link: "https://github.com/ubports/ubuntu-touch/issues"
    icon: "github"
  - name: "Device source"
    link: "https://github.com/"
    icon: "github"
docLinks:
  - name: "How to flash the os on a SD card"
    link: "https://docs.ubports.com"
---
```
